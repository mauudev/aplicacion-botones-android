package com.tutorial1.botones_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Toast;
import android.media.MediaPlayer;
import android.widget.ToggleButton;



public class MainActivity extends Activity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.sound);
        final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.perereca);
        Button btAceptar = (Button) findViewById(R.id.btAceptar);
        Button btCancelar = (Button) findViewById(R.id.btCancelar);
        Button bt1 = (Button) findViewById(R.id.bt1);
        Button bt2 = (Button) findViewById(R.id.bt2);
        Button bt3 = (Button) findViewById(R.id.bt3);
        Button btLayouts = (Button) findViewById(R.id.btLayouts);
        Button btSel = (Button) findViewById(R.id.btCtrlSel);
        Button btSpinner = (Button) findViewById(R.id.btSpinner);
        Button btRadio = (Button) findViewById(R.id.btRadio);
        Button btCtrlTexto = (Button) findViewById(R.id.btCtrlTexto);
        ImageButton btCarla = (ImageButton) findViewById(R.id.btCarla);
        Button btAndroid = (Button) findViewById(R.id.btAndroidButton);
        CheckBox cb = (CheckBox) findViewById(R.id.btCheck);
        ToggleButton tb = (ToggleButton) findViewById(R.id.btToggle);
        Button btGridView = (Button) findViewById(R.id.btGridView);

        btAceptar.setOnClickListener(new View.OnClickListener() { //mostrar texto forma 1
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Has pulsado el boton Aceptar :)", Toast.LENGTH_SHORT).show();
            }
        });

        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
        bt3.setOnClickListener(this);
        btAndroid.setCompoundDrawablesWithIntrinsicBounds(
                0, //left
                R.mipmap.ic_launcher, //top
                0, //right
                0  //bottom
        );
        tb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    Toast.makeText(getApplicationContext(), "Boton activo!", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getApplicationContext(), "Boton inactivo!", Toast.LENGTH_SHORT).show();
            }
        });
        btCarla.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getApplicationContext(), "Pulsacion larga, hazlo corta para que carlita gima! ;)", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        btCarla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.tutorial1.botones_app.CarlaActivity"));
                mp.start();
            }
        });
        btSel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.tutorial1.botones_app.ControlesSeleccionActivity"));
            }
        });
        btGridView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.tutorial1.botones_app.GridViewActivity"));
            }
        });
        btSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.tutorial1.botones_app.SpinnerActivity"));
            }
        });
        btRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.tutorial1.botones_app.RadioButtonActivity"));
            }
        });
        btCtrlTexto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.tutorial1.botones_app.ControlesDeTextoActivity"));
            }
        });
        btLayouts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.tutorial1.botones_app.LayoutsActivity"));
            }
        });
        btAndroid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mp2.isPlaying()) {
                    mp2.pause();
                    mp2.seekTo(0);
                } else
                    try {
                        mp2.seekTo(0);
                        mp2.start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        });
    }

    @Override
    public void onClick(View v){
        String str = "";
        switch(v.getId()){
            case R.id.bt1:
                str = "Boton 1";
                break;
            case R.id.bt2:
                str = "Boton 2";
                break;
            case R.id.bt3:
                str = "Boton 3";
                break;
        }
        Toast.makeText(getBaseContext(), "Has pulsado el " + str +" :)", Toast.LENGTH_SHORT).show();
    }
    public void clickCancelar(View v){//mostrar texto forma 2
        Toast.makeText(getBaseContext(), "Has pulsado el boton Cancelar :)", Toast.LENGTH_SHORT).show();
    }

    public void onClickCheckBox(View v){
        boolean flag = ((CheckBox)v).isChecked();
        if(flag){
            Toast.makeText(getApplicationContext(),"Boton activo!",Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
