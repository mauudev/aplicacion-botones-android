package com.tutorial1.botones_app;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.MultiAutoCompleteTextView;


public class ControlesDeTextoActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controles_de_texto);

        AutoCompleteTextView auto = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView);
        MultiAutoCompleteTextView multi = (MultiAutoCompleteTextView)findViewById(R.id.multiAutoCompleteTextView);

        String[] paises = getResources().getStringArray(R.array.paises_array);//puede ser una base de datos

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,paises);//poner una lista desplegable para mostrar los paises del array

        auto.setThreshold(1);//muestra cantidad de caracteres para sugerir un pais
        multi.setThreshold(1);
        multi.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());//para usar comasen el texto

        auto.setAdapter(adapter);
        multi.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_controles_de_texto, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}